module.exports = {
    printWidth: 120,
    bracketSameLine: true,
    // htmlWhitespaceSensitivity: ignore,
    tabWidth: 4,
};
