import '../styles/globals.css'
import '../styles/if.css'

// Default Export
export default function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
  }